let mongoose = require('mongoose');
let mongoosePaginate = require('mongoose-paginate');
let Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;
let UserSchema = mongoose.Schema({

    type:{
        type:Number
    },
    name: {
        type: String
    },
    firstName:{
        type:String
    },
    lastName:{
        type:String
    },
    email: {
        type: String
    },
    password: {
        type: String
    },
    mobile: {
        type: String
    },
    birthdate: {
        type: Date
    },
    age: {
        type: String
    },
    profile: {
        type: String
    },
    zodiacSign: {
        type: String
    },
    gradeLevel: {
        type: String
    },
    createdDate: {
        type: Date
    },
    updatedDate: {
        type: Date
    },
    //who generate user/super admin/admin/tutor
    createdBy: {
        type: ObjectId,
        ref: 'User'
    },
    //which student assigned to which tutor
    tutorId: {
        type: ObjectId,
        ref: 'User'
    },
    forgotPasswordCode:{
        type:String
    }

});

UserSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('user', UserSchema);